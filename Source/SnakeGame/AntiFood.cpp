#include "SnakeBase.h"
#include "AntiFood.h"
#include <Kismet/KismetMathLibrary.h>

void AAntiFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval(Snake->MovementSpeed = 0.5);
		
			if (UKismetMathLibrary::RandomBoolWithWeight(0.1))
			{
				Snake->AddRandomAntiFood(); 
				
			}					
			Destroy();
		}
	}
}
