#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "AntiFood.generated.h"

UCLASS()
class SNAKEGAME_API AAntiFood : public AFood
{
	GENERATED_BODY()

public:
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
