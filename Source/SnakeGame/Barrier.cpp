#include "Barrier.h"
#include "SnakeBase.h"

ABarrier::ABarrier()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABarrier::BeginPlay()
{
	Super::BeginPlay();
}

void ABarrier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABarrier::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Game Over!"));
	}
}

