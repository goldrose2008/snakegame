#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Barrier.generated.h"

UCLASS()
class SNAKEGAME_API ABarrier : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ABarrier();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
