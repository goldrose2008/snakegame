#include "SnakeBase.h"
#include "Food.h"
#include <Kismet/KismetMathLibrary.h>


AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();
}

void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->AddRandomFood();
			if (UKismetMathLibrary::RandomBoolWithWeight(0.3))
			{
				Snake->AddRandomAntiFood();
			}	
			Destroy();
		}
	}
}