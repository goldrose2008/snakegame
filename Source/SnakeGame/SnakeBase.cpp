#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MovementSpeed = 0.2;
	LastMoveDirection = EMovementDirection::DOWN;
	MinX = -100.f;
	MaxX = 100.f;
	MinY = -100.f;
	MaxY = 100.f;
	PointZ = 0.f;
}

void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	AddRandomFood();
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
		else
		{
			NewSnakeElem->MeshComponent->SetVisibility(false);
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;

	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;

	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;

	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	} 

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		SnakeElements[i]->MeshComponent->SetVisibility(true);
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InterctableInterface = Cast<IInteractable>(Other);
		if(InterctableInterface)
		{
			InterctableInterface->Interact(this, bIsFirst);
		}
	}
}

FVector ASnakeBase::SearchRandomPoint()
{
	float RandomPointX = FMath::FRandRange(MinX, MaxX);
	float RandomPointY = FMath::FRandRange(MinY, MaxY);

	FVector StartPoint = FVector(RandomPointX, RandomPointY, PointZ);
	return StartPoint;
}

void ASnakeBase::AddRandomFood()
{
	FRotator StartFoodRotation = FRotator(0, 0, 0);
	FVector StartPointFood = SearchRandomPoint();
	
	GetWorld()->SpawnActor<AFood>(FoodActorClass, StartPointFood, StartFoodRotation);
}

void ASnakeBase::AddRandomAntiFood()
{
	FRotator StartAntiFoodRotation = FRotator(0, 0, 0);
	FVector StartPointAntiFood = SearchRandomPoint();

	GetWorld()->SpawnActor<AAntiFood>(AntiFoodActorClass, StartPointAntiFood, StartAntiFoodRotation);
}
