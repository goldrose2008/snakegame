#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Food.h"
#include "AntiFood.h"
#include "SnakeBase.generated.h"

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class ASnakeElementBase;

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AAntiFood> AntiFoodActorClass;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;
	UPROPERTY()
		EMovementDirection LastMoveDirection;
	
	UPROPERTY(EditDefaultsOnly)
		float ElementSize;
	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;
	UPROPERTY(EditDefaultsOnly)
		float MinX;
	UPROPERTY(EditDefaultsOnly)
		float MaxX;
	UPROPERTY(EditDefaultsOnly)
		float MinY;
	UPROPERTY(EditDefaultsOnly)
		float MaxY;
	UPROPERTY(EditDefaultsOnly)
		float PointZ;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	    void AddSnakeElement(int ElementsNum = 1);
	
	UFUNCTION(BlueprintCallable)
	    void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	FVector SearchRandomPoint();
	void AddRandomFood();
	void AddRandomAntiFood();
};
